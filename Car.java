package vehciles;

public class Car extends Vehicle {

private int numberofDoors;

public Car(String make, String model, int year, int numWheels, int numberofDoors){
    super(make, model, year, numWheels);
    this.numberofDoors = numberofDoors;
}

public int getNumberofDoors() {
    return this.numberofDoors;
}

public void setNumberofDoors(int numberofDoors) {
     this.numberofDoors = numberofDoors;
}
@Override
public String toString(){
    return super.toString() + "\nNumber of Door: " + this.numberofDoors;
}
}